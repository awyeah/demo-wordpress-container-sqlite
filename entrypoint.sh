#!/usr/bin/env bash

set -ex

# Restore the database if it does not already exist.
if [ -f /var/www/html/wp-content/database/db ]; then
    echo "Database already exists, skipping restore"
else
    echo "No database found, restoring from replica if exists"
    mkdir -p /var/www/html/wp-content/database
    litestream restore -v -if-replica-exists -config /etc/litestream.yml /var/www/html/wp-content/database/db
    chown -R www-data:www-data /var/www/html/wp-content/database
fi

# Run litestream with your app as the subprocess.
# docker-entrypoint.sh is copied by the wordpress base image and set as the ENTRYPOINT
# apache2-foreground is the default CMD
exec litestream replicate -config /etc/litestream.yml -exec "docker-entrypoint.sh apache2-foreground"

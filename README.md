# Wordpress Containerized with SQLite

The theme is SKT Software, which I found while searching for a free Wordpress
theme. There's nothing unique about it that I'm aware of - I just needed a theme
for the demo.

- [Download Link](https://downloads.wordpress.org/theme/skt-software.3.0.zip)
- [Found here
  2021-10-18](https://www.sktthemes.org/wordpress-themes/free-wordpress-themes/)

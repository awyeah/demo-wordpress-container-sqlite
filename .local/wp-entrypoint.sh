#!/bin/sh

until curl -f http://localhost:9000/minio/health/live; do
    >&2 echo "minio api not available - sleeping"
    sleep 1
done

>&2 echo "minio api available; running entrypoint"
/scripts/entrypoint.sh
